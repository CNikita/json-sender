const express = require('express')
const app = express()

const conf = require('./conf')
const routes = require('./routes')

const port = conf.PORT || 3000

app.use('/api', routes)

app.listen(port, () => {
    console.log("Server started")
})